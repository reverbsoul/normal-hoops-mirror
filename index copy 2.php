<?php get_header(); ?>

	<div class="container">
		<!-- Nivo Slider -->
		<div class="hidden columns two">
		</div>
		<div class="slider-wrapper columns fourteen">
			<div id="slider" class="nivoSlider">
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-1.jpg" alt="" title="Welcome to Normal Hoops!" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-2.jpg" alt="" title="Tips for Beginners!" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-3.jpg" alt="" title="Hoop Yoga Instructionals" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-4.jpg" alt="" title="Sunday Spinday" />
			</div>
		</div>
		
		<div id="htmlcaption" class="nivo-html-caption">
			<strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
		</div>
			<script type="text/javascript">
				if (screen.width >= 450) {
					jQuery(window).load(function() {
						jQuery('#slider').nivoSlider();
				});	}
			</script>
			
		<!-- End Nivo Slider -->
		
		
		<div class="hidden columns two">
		</div>
		<div class="columns fourteen">
			<?php if (have_posts()) : ?>
			<ul class="row featured-section">
			<?php while(have_posts()) : the_post(); ?>	
				<li class="columns four">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>			
					<p><?php the_excerpt(); ?></p>	
				</li>
			<?php endwhile; ?>	
			</ul><!--/ row-->
			
			<?php else: ?>
			<p>No posts were found.</p>
			<?php endif ?>	
		</div>


<?php get_footer(); ?>
