<?php get_header(); ?>
<div class="container">
	<div class="hidden columns two">
	</div>
	<div class="single-page columns fourteen">
		<ul class="featured-section">
		<?php if (have_posts()) : while(have_posts())	: the_post(); ?>
			<li>
				<?php the_post_thumbnail(); ?>
				<div class="content"><?php the_content(); ?></div>
			</li>
		</ul>
		<?php endwhile; endif; ?>

</div>



<?php get_footer(); ?>