<?php 
 /* Template Name: Contact Page */
?>

<?php get_header(); ?>
<div class="container">
	<div class="hidden columns two">
	</div>
	<div class="contact-page columns fourteen">
	<ul class="row featured-section">
		<li>
		<?php echo do_shortcode( '[contact-form-7 id="34" title="Contact form 1"]' ); ?>
		</li>
	</ul>
	</div>
<?php get_footer(); ?>
