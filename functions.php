<?php
/********************************************************************/
/* Define Constants */
/********************************************************************/
define('THEMEROOT' , get_stylesheet_directory_uri());
define('IMAGES' , THEMEROOT.'/img');


/********************************************************************/
/* Add Menus */
/********************************************************************/
function register_my_menus() {
	register_nav_menus(array(
		'main-menu' => 'Main Menu'
	));
}

add_action('init', 'register_my_menus');

/********************************************************************/
/* Add Theme Support for Post Thumbnails */
/********************************************************************/
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(1225, 470, true );
}

?>