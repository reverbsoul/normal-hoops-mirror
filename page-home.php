<?php 
 /* Template Name: Homepage */
?>

<?php get_header(); ?>

	<div class="container">
		<!-- Nivo Slider -->
		<div class="hidden columns two">
		</div>
		<div class="slider-wrapper columns fourteen">
			<div id="slider" class="nivoSlider">
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-1.jpg" alt="" title="Welcome to Normal Hoops!" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-2.jpg" alt="" title="Create your well-being in the hoop!" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-3.jpg" alt="" title="Explore" />
				<img src="<?php bloginfo('template_url'); ?>/img/blog-post-4.jpg" alt="" title="Have fun!" />
			</div>
		</div>
			<script type="text/javascript">
				if (screen.width >= 450) {
					jQuery(window).load(function() {
						jQuery('#slider').nivoSlider();
				});	}
			</script>
			
		<!-- End Nivo Slider -->
		
		
<!-- The Newest 4 Events -->		
		
		
		
		<div class="hidden columns two">
		</div>
		<div class="home-page columns fourteen">
			<ul class="row featured-section">
			<?php
			query_posts( array ( 'category_name' => 'events', 'posts_per_page' => 4 ) ); 
			while (have_posts()) : the_post(); ?>
			
					<li class="columns four">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>			
						<p><?php the_excerpt(); ?></p>
						<a href="<?php the_permalink(); ?>"><p class="read-more"> Read More...</p></a>	
					</li>
			<?php endwhile; ?>
			</ul>
		</div>		
	
	

<?php get_footer(); ?>
