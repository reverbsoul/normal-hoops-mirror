<?php 
 /* Template Name: Events Page */
?>

<?php get_header(); ?>

<div class="container">	
	<div class="hidden columns two">
		</div>
		<div class="events-page columns fourteen">
			<ul class="row featured-section">
			<?php if (have_posts()) : ?>
			<?php query_posts(array('category_name=events', 'posts_per_page' => 4) ); 
		while (have_posts()) : the_post(); ?>
				<li class="columns four">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>			
					<p><?php the_excerpt(); ?></p>	
					<a href="<?php the_permalink(); ?>"><p class="read-more"> Read More...</p></a>
				</li>
				<?php endwhile; ?>
				<?php else: ?>
				<p>No posts were found.</p>
				<?php endif ?>
				<li class="older"><h3><?php next_posts_link('« Older Events') ?></h3></li>
				<li class="newer"><h3><?php previous_posts_link('Newer Events »') ?></h3></li>					
			</ul> <!--/ row-->
		</div>



<?php get_footer(); ?>
