<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=""> <!--<![endif]-->
<html>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">	

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
	<meta name=description content="<?php bloginfo('description'); ?>">
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<?php wp_enqueue_script("jquery"); ?>
	
	<!-- Fonts Import -->
	<link href='http://fonts.googleapis.com/css?family=Days+One' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Overlock' rel='stylesheet' type='text/css'>
	<!-- End Font Import -->
	
	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<!-- Nivo Slider Initialize -->
	<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/jquery.nivo.slider.pack.js"></script>
	<!-- End Nivo Slider -->
</head>
<body <?php body_class(); ?>>
			<div class="">
				<h1 class="large-logo">
					<a href="<?php echo home_url(); ?>">Normal Hoops</a>
				</h1>
			</div>

	<div class="container">
	
		<header class="row">
				

			<div class="hidden columns two">
			</div>
			<div class="columns fourteen">
			
				<h1 class="logo">
					<a href="<?php echo home_url(); ?>">Normal Hoops</a>
				</h1>

					
				<a href="#navigation" class="nav-link">Menu</a>	
					
			</div>
			<div class="hidden columns two">
			</div>
			<div class="columns fourteen">
				<nav role="primary" id="">

						<?php
						wp_nav_menu(array(
							'theme_location' => 'main-menu',
							'container' => '',
							'menu_class' => ''
							));
						?>
												
				</nav>
			</div>
		
		</header><!--/ row-->
		
	</div><!--/ container-->