<?php 
 /* Template Name: About Page */
?>

<?php get_header(); ?>
<div class="container">
	<div class="hidden columns two">
	</div>
	<div class="about-page columns fourteen">
		<h3>About Normal Hoops</h3>
		<p>
		My dedication to fitness began at the age of three when I started competitive swimming.  My swimming career carried me through college where I earned a degree in the fine arts.  After school, I found an outlet to satisfy my inner athlete, and a way to carry on a fit lifestyle through the hula hoop.
		</p>
		<p>
		My fixation with the hoop started when I witnessed a hoop dancer grooving at a music festival in the summer of 2002.  In that moment, I vowed to learn whatever she was doing.  The following summer that I purchased my first hoop.  It was so big it didn’t fit in my car, so a friend agreed to transport it home in his microbus.  
		</p>
		<p>
		Once I had my hoop, I was hooked.  For years, I was a waist hooper who loved nothing more than grooving to the music.  Then, in 2008, I saw another hoop dancer and realized I could take it farther.  After making a more reasonably sized hoop, I started teaching myself tricks.  It was the satisfaction of learning each new trick that started my serious addiction to hoop dancing. 
		</p>
		<p>
		The thing that draws me to hooping is the combination of cardio and strength training, which can be done (amazingly) with a huge smile on your face.  Working out in the pool is a solitary act, and a great form of meditation, but with hooping, I have come to enjoy (and feed off) the connection with others who share the same passion.  A growing network of hoopers strengthens the connection between each other and to the hoop.  The hula hoop is not only a toy, but a tool.  Hooping is a way of life that encourages having fun while promoting both physical and mental strength.  
		</p>
		<p>
		The hoop symbolizes the ability to play, learn, laugh, and connect within the circle.  The Zen of the hoop has forged a new path in my life.  Within the hoop, I have found the peace and confidence that allows an athlete to become a dancer.  My goal is to help people find out what the hoop can do for them, and in doing so, to strengthen and widen our local hooping community.  
		</p>
		<h3>
		Viva La Hoop!
		</h3>
	
		<ul>
			<li><p>Playing in the hoop for 10 years</p></li>
			<li><p>Enjoys spreading the joy of hooping in hometown</p></li>
			<li><p>Teaching in blo/no 4 years</p></li>
			<li><p>Founder/owner of Normal hoops</p></li>
			<li><p>Performs at local music events</p></li>
			<li><p>Former athlete turned hooper who enjoys the physical aspect of hoop dance and bringing play back to the lives of adults.</p></li>
			<li><p>Live music and sustained spinning is what I feed off of most.</p></li>
		</ul> 
	</div>

<?php get_footer(); ?>
