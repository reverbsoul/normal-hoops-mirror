<?php 
 /* Template Name: Gallery Page */
?>

<?php get_header(); ?>
<div class="container">
		<div class="hidden columns two">
		</div>
		<div class="gallery-page columns fourteen">
		<ul class="row featured-section">
		<?php
		query_posts('category_name=gallery'); 
		while (have_posts()) : the_post(); ?>
				<li class="columns sixteen">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>			
					<a href="<?php the_permalink(); ?>"><p><?php the_excerpt(); ?></p></a>
				</li>
		<?php endwhile; ?>
		</ul>
</div>



<?php get_footer(); ?>
